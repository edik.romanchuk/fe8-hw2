const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const del = require("del");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const minifyjs = require("gulp-js-minify");
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create();
const reload = browserSync.reload;
const imagemin = require("gulp-imagemin");

gulp.task("prepare-html", (cb) => {
  gulp.src("src/*.html").pipe(gulp.dest("dist"));
  cb();
});

gulp.task("clone-reset", (cb) => {
  gulp.src("./src/scss/libs/*.css").pipe(gulp.dest("./dist/css/libs"));
  cb();
});

gulp.task("prepare-css", (cb) => {
  gulp
    .src("./src/scss/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(concat("styles.min.css"))
    .pipe(gulp.dest("./dist/css/"));

  cb();
});
gulp.task("prepare-js", (cb) => {
  gulp
    .src("src/js/*.js")
    .pipe(concat("/js/scripts.min.js"))
    .pipe(minifyjs())
    .pipe(gulp.dest("dist"));

  cb();
});

gulp.task("image-min", (cb) => {
    gulp.src("src/images/*")
        .pipe(imagemin())
        .pipe(gulp.dest("dist/images"))
  
    cb();
  });

gulp.task("clean", (cb) => {
  del("dist/**");
  cb();
});

gulp.task("server", function () {
  browserSync.init({
    server: "./dist",
  });

  gulp.watch("src/*.html", gulp.series("prepare-html"));
  gulp.watch("dist/index.html").on("change", reload);
  gulp.watch("./src/scss/**/*.scss", gulp.series("prepare-css"));
  gulp.watch("dist/styles.min.css").on("change", reload);
  gulp.watch("./src/js/*.js", gulp.series("prepare-js"));
  gulp.watch("dist/scripts.min.js").on("change", reload);
});

exports.build = gulp.series("clean", "prepare-html", "prepare-css", "prepare-js", "image-min", "clone-reset");
exports.dev = gulp.series("server");
